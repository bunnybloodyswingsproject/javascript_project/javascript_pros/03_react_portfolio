const User = require("../models/User");
const jwt = require("jsonwebtoken");
const mongoose = require("mongoose");

const refresh = async (req, res) => {
    // console.log("refresh");
    const cookies = req.cookies;
    if(!cookies.jwt) {
        return res.status(401)
                  .send({
                    "success": false,
                    "message": "Cannot find the users token"
                  });
    }
    const refreshToken = cookies.jwt;
    const foundUser = await User.find({token: refreshToken});
    if(!foundUser) {
        return res.status(403)
                  .send({
                    "success": false,
                    "message": "Cannot find the users token"
                  });
    }
    jwt.verify(
        refreshToken,
        process.env.REFRESH_KEY,
        (err, decoded) => {
            if(err || foundUser._id !== decoded._id) {
                return res.status(403)
                   .send({
                    "status": false,
                    "message": "Cannot decode user for refreshing token"
                   });
            }
            const token = jwt.sign(
                {"_id": decoded._id},
                process.env.SECRET_KEY,
                {expiresIn: "1h"}
            );

            res.json({token});
        }
    )
}

const persistAuth = async (req, res) => {
    const cookies = req.cookies;
    if(!cookies?.jwt) {
        return res.status(401)
                  .send({
                    "success": false,
                    "message": "Cannot find the users token"
                  });
    }
    const refreshToken = cookies.jwt;
    jwt.verify(
        refreshToken,
        process.env.REFRESH_KEY,
        async (err, decoded) => {
            if(err) {
                return res.status(403)
                   .send({
                    "status": false,
                    "message": "Cannot decode user for refreshing token"
                   });
            }
            const user = await User.findById({
                _id: decoded.userId
            });
            res.send({
                success: true,
                user
            });
        }
    )
}

const addGoals = async (req, res) => {
    const newId = new mongoose.Types.ObjectId();
    const {
        task,
        description,
        progress,
        thumbnail
    } = req.body;
    try {
        await User.findOneAndUpdate({
            _id: req.body.userId
        }, {
            $push: {
                "goals": {
                    _id: newId,
                    task: task,
                    progress: progress,
                    date_started: new Date(),
                    deadline_date: new Date("2023-12-31"),
                    description: description,
                    thumbnail: thumbnail
                }
            }
        });

        return res
                .status(201)
                .send({
                "success": true,
                "message": "Successfully added a new goals",
                newId
        })

    }catch(error) {
        console.log(error);
        return res.send({
            success: false,
            message: "Somethings wrong with adding goals functionalities."
        })
    }
}

const getGoals = async (req, res) => {
    try {
        const goals = await User.findOne({
            _id: req.query.userId
        });

        if(!goals) {
            return res.send({
                success: false,
                message: "Failure on fetching goals cause user is not defined."
            });
        }

        return res.send({
            success: true,
            message: "Successfully fetched all user's goals.",
            goals: goals.goals
        });
    }catch(error) {
        console.log(error);
        return res.send({
            success: false,
            message: "Failure on fetching goals data."
        })
    }
}

const updateGoals = async (req, res) => {
    const {
        userId,
        goalId,
        task,
        description,
        progress,
        thumbnail
    } = req.body;
    try {
        await User.updateOne({
            $and: [{
                _id: mongoose.Types.ObjectId(userId)
            }, {
                "goals._id": mongoose.Types.ObjectId(goalId)
            }]
        }, {
            $set: {
                "goals.$.task": task,
                "goals.$.description": description,
                "goals.$.progress": progress,
                "goals.$.thumbnail": thumbnail
            }
        });

        return res.send({
            success: true,
            message: "A goal has been updated."
        });

    }catch(error) {
        console.log(error);
        return res.send({
            success: false,
            message: "Failure on update goals data."
        });
    }
}

const deleteGoal = async (req, res) => {
    try {
        const {
            userId,
            goalId
        } = req.body;
        const something = await User.findOneAndUpdate({
            _id: mongoose.Types.ObjectId(userId)
        }, {
            $pull: {
                "goals": {
                    "_id": mongoose.Types.ObjectId(goalId)
                }
            }
        });
        res.send({
            "success": true,
            "message": "Successfully deleted a goal."
        });
    }catch(error) {
        console.log(error);
        return res.send({
            success: false,
            message: "There's something wrong with delete goal functionality."
        })
    }
}

module.exports = {
    refresh,
    persistAuth,
    addGoals,
    getGoals,
    updateGoals,
    deleteGoal
}