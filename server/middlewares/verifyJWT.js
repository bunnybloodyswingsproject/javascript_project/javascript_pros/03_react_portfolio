const jwt = require("jsonwebtoken");

const verifyJWT = (req, res, next) => {
    const authHeader = req.headers["authorization"];
    if(!authHeader) return res.status(401).send({"message": "Invalid token"});
    const token = authHeader.split(" ")[1];
    jwt.verify(
        token,
        process.env.SECRET_KEY,
        (err, decoded) => {
            if(err) return res.status(403).send({"message": "Unauthorized personnel"});
            req.user = decoded.userId;
            next();
        }
    )
}

module.exports = verifyJWT;