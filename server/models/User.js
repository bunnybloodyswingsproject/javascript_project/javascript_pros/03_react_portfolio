const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

const UserSchema = new mongoose.Schema({
    fullname: {
        type: String,
        required: [true, "Please provide your name."],
        maxLength: 50,
        minLenth: 10
    },
    email: {
        type: String,
        required: [true, "Please provide email."],
        unique: true
    },
    password: {
        type: String,
        required: [true, "Please provide password."],
        minLenth: 6
    },
    image: {
        type: String,
        default: ""
    },
    isAdmin: {
        type: Boolean,
        default: false
    },
    goals: [{
        _id: mongoose.Types.ObjectId,
        task: {
            type: String,
            minlength: 6
        },
        progress: String,
        date_started: Number,
        deadline_date: Number,
        description: String,
        thumbnail: String
    }]
}, { timestamps: true });

UserSchema.pre("save", async function() {
    const salt = await bcrypt.genSalt(10);
    this.password = await bcrypt.hash(this.password, salt);
});

UserSchema.methods.comparePassword = async function(userPassword) {
    const isMatch = await bcrypt.compare(userPassword, this.password);
    return isMatch;
}

UserSchema.methods.createJWT = function () {
    return jwt.sign(
        {userId: this._id, isAdmin: this.isAdmin},
        process.env.SECRET_KEY,
        {expiresIn: "1h"}
    );
}

UserSchema.methods.refreshJWT = function () {
    return jwt.sign(
        {userId: this._id, isAdmin: this.isAdmin},
        process.env.REFRESH_KEY,
        {expiresIn: "1h"}  
    );
}

module.exports = mongoose.model("User", UserSchema);


