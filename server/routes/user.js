const router = require("express").Router();
const verifyJWT = require("../middlewares/verifyJWT");

const {
    // findUser,
    // userUpdate,
    refresh,
    persistAuth,
    addGoals,
    getGoals,
    updateGoals,
    deleteGoal
} = require("../controllers/user");

router.get("/refresh", refresh);
router.get("/persistingauth", persistAuth);
router.post("/addgoals", verifyJWT, addGoals);
router.get("/getgoals", verifyJWT, getGoals);
router.post("/updategoals", verifyJWT, updateGoals);
router.post("/deletegoal", verifyJWT, deleteGoal);

module.exports = router;
