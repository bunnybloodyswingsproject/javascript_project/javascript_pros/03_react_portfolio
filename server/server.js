require("dotenv").config();
const express = require("express");
const app = express();
const connectDB = require("./db/connection");
const authRoutes = require("./routes/route");
const userRoutes = require("./routes/user");
const cookieParser = require("cookie-parser");
const bodyParser = require("body-parser");
const cors = require("cors");

app.use(express.json());
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(bodyParser.json());
app.use(cookieParser());
const corsOptions ={
    origin:'https://cl-portfolio-anthonies.onrender.com', 
    credentials:true,            //access-control-allow-credentials:true
    optionSuccessStatus:200
}
app.use(cors(corsOptions));


app.use("/api/auth", authRoutes);
app.use("/api/user", userRoutes);

app.listen(8000, () => {
    // For the sake of this assestment review, MongoDB URI is expose.
    connectDB(process.env.MONGO_URI);
    console.log("Backend is running");
})