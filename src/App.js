import './App.css';
import Home from './pages/home/Home';
import { BrowserRouter as Router, Routes, Route} from "react-router-dom";
import Layout from "./components/Layout";
import Login from "./pages/login/Login";
import PersistLogin from "./components/PersistLogin";
import RequireAuth from "./components/RequireAuth";

function App() {
  return (
    <div className="App">
      <Router>
        <Routes path="/" element={<Layout /> }>
          {/* Public Routes */}
          <Route path="login" element={<Login />} />
          {/* Restricted Routes */}
          <Route element={<PersistLogin />}>
            <Route element={<RequireAuth /> }>
              <Route path="/" element={<Home />} />
            </Route>
          </Route>
        </Routes>
      </Router>
    </div>
  );
}

export default App;
