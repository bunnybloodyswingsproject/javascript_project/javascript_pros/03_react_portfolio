import React from 'react'

const Footer = () => {
  return (
    <div className="footer__container">
        <div className="footer_content_container" id="footer_content_container">
            <div className="footer_image_container">
                <img src="https://firebasestorage.googleapis.com/v0/b/netflix-f5fc3.appspot.com/o/batugst_file_uploader%2Fimage%201avatar.png?alt=media&token=55385648-87e9-400a-a031-4c37b7400426" alt="footer__avatar" />
            </div>
            <div className='footer_greetings_header'>
                <h2>YOU MADE IT!</h2>
            </div>
            <div className='footer__closing_remarks'>
                <p>
                Thanks for checking out my page. For inquiry, hiring officer, or collaboration. Please contact me at <span href="cyrilelagumbay@gmail.com">cyrilelagumbay@gmail.com</span>. You can also reach me at linkedIn.
                </p>
            </div>
            <div className='copyright_footer'>
                <small>© 2020 created using REACTJS by Cyrile Lagumbay.</small>
            </div>
        </div>
    </div>
  )
}

export default Footer