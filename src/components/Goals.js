import React, { useEffect, useRef, useState } from 'react'
import {motion} from "framer-motion";
import storage from "../firebase";
import { ToastContainer, toast } from 'react-toastify';
import {ref, uploadBytesResumable, getDownloadURL} from "firebase/storage";
import  useAxiosPrivate from "../hooks/useAxiosPrivate";
import GoalsForm from './GoalsForm';
import useGoals from "../hooks/useGoals";

const Goals = ({user, setUser}) => {
    const [width, setWidth] = useState(0);
    const [isOpenForm, setIsOpenForm] = useState(false);

    const carousel = useRef();
    const [status, setStatus] = useState("");
    const [aim, setAim] = useState({});
    const axiosPrivate = useAxiosPrivate();
    const [isAddGoals, setIsAddGoals] = useState(true);
    const { goals, setGoals } = useGoals();
    const goalsRef = useRef(false);
    const [updateGoals, setUpdateGoals] = useState(null);
    useEffect(() => {
        // console.log(carousel.current.scrollWidth, carousel.current.offsetWidth)
        setWidth(carousel.current?.scrollWidth - carousel.current?.offsetWidth)
    }, []);

    const onhandleFormSelect = (e) => {
        setAim({...aim, "progress": e.target.value});
    }

    const onUpdateFormSelect = (e) => {
        setStatus(e.target.value);
        setUpdateGoals({...updateGoals, progress: e.target.value});
    }

    const onhandleGoalInputs = (e) => {
        const value = e.target.value;
        setAim({...aim, [e.target.name]: value});
    }

    const limitContent = (string, limit) => {
        return string?.substring(0, limit) + "...";
    }

    const dateFormatConverter = (date) => {
        const months = [
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December"
        ]
        const month = months[new Date(date).getMonth()];
        const year = new Date(date).getFullYear();
        const day = new Date(date).getDate();
      
        return `${month} ${day}, ${year}`
    }

    useEffect(() => {
        const fetchedGoals = async () => {
            try {
                const response = await axiosPrivate.get("user/getgoals", {
                    params: { userId: "63c5452695acf197018e4921" }
                },
                {
                    headers: { "Content-Type": "application/json" },
                    withCredentials: true
                });

                if(!response.data.success) {
                    toast.error(<p className="login__error_msg">{response.data.message}</p>, {
                        position: "top-right",
                        autoClose: 5000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true,
                        progress: undefined,
                        theme: "light",
                      });
                      return; 
                }

                setGoals(response.data.goals);
            }catch(error) {
                console.log(error);
                toast.error(<p className="login__error_msg">{error}</p>, {
                    position: "top-right",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                    theme: "light",
                  });
                  return;
            }
        }

        goalsRef.current && fetchedGoals();

        return () => goalsRef.current = true;
    }, [axiosPrivate, setGoals]);

    const upload = (items) => {
        items.forEach(item => {
            const filename = new Date().getTime() + item.label + item.file.name;
            const storageRef = ref(storage, "/batugst_file_uploader/" + filename);
            const uploadTask = uploadBytesResumable(storageRef, item.file);
            uploadTask.on("state_changed", (snapshot) => {
                let progress = Math.round((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
                console.log(snapshot);
                console.log(`Upload is ${progress}% done`);
                switch(snapshot.state) {
                    case "paused":
                        console.log("Upload is pause");
                        break;
                    case "running":
                        console.log("Upload is running");
                        break;
                    default:
                        console.log("Uploading the files....");
                        break;
                }
            },
            (err) => {
                toast.error(<p className="login__error_msg">{err}</p>, {
                    position: "top-right",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                    theme: "light",
                  });
                  return;
            },
            () => {
                getDownloadURL(uploadTask.snapshot.ref).then(url => {
                    setAim({...aim, [item.label]: url});
                })
            })
        })
    }

    const updateUpload = (items) => {
        items.forEach(item => {
            const filename = new Date().getTime() + item.label + item.file.name;
            const storageRef = ref(storage, "/batugst_file_uploader/" + filename);
            const uploadTask = uploadBytesResumable(storageRef, item.file);
            uploadTask.on("state_changed", (snapshot) => {
                let progress = Math.round((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
                console.log(snapshot);
                console.log(`Upload is ${progress}% done`);
                switch(snapshot.state) {
                    case "paused":
                        console.log("Upload is pause");
                        break;
                    case "running":
                        console.log("Upload is running");
                        break;
                    default:
                        console.log("Uploading the files....");
                        break;
                }
            },
            (err) => {
                toast.error(<p className="login__error_msg">{err}</p>, {
                    position: "top-right",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                    theme: "light",
                  });
                  return;
            },
            () => {
                getDownloadURL(uploadTask.snapshot.ref).then(url => {
                    setUpdateGoals({...updateGoals, [item.label]: url});
                })
            })
        })
    }


    const handleUpload = (e) => {
        e.preventDefault();
        if(e.target.files[0] === "") {
            toast.error(<p className="login__error_msg">Please upload a video.</p>, {
              position: "top-right",
              autoClose: 5000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true,
              progress: undefined,
              theme: "light",
            });
            return;
        }

        upload([
            {file: e.target.files[0] , label: "thumbnail"}
        ])
    }

    const handleGoalsUploadUpdate = (e) => {
        e.preventDefault();
        if(e.target.files[0] === "") {
            toast.error(<p className="login__error_msg">Please upload a video.</p>, {
              position: "top-right",
              autoClose: 5000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true,
              progress: undefined,
              theme: "light",
            });
            return;
        }

        updateUpload([
            {file: e.target.files[0] , label: "thumbnail"}
        ])
    }
    
    
    const onhandleCreateGoals = async (e) => {
        e.preventDefault();
        if(!user.isAdmin) {
            toast.error(<p className="login__error_msg">User is not authorized to do this transaction.</p>, {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
                theme: "light",
              });
              return;
        }

        if(
            aim.task === undefined || 
            aim.task === "" ||
            aim.progress === undefined ||
            aim.description === undefined ||
            aim.description === "" ||
            aim.thumbnail === null
        ) {
            toast.error(<p className="login__error_msg">Please fill up all the required fields.</p>, {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
                theme: "light",
              });
              return;
        }

        try {
            const response = await axiosPrivate.post("user/addgoals",
            JSON.stringify({
                "userId": user._id,
                "task": aim?.task,
                "description": aim?.description,
                "progress": aim?.progress,
                "thumbnail": aim?.thumbnail
            }),
            {
                headers: { "Content-Type": "application/json" },
                withCredentials: true
            }
            );

            if(!response.data.success) {
                toast.error(<p className="login__error_msg">{response.data.message}</p>, {
                    position: "top-right",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                    theme: "light",
                  });
                  return;
            }

            setGoals([...goals, 
                {
                    _id: response.data.newId,
                    task: aim?.task,
                    description: aim?.description,
                    progress: aim?.progress,
                    date_started: dateFormatConverter(new Date()),
                    deadline_date: "January 31, 2023",
                    thumbnail: aim?.thumbnail
                }
            ]);

            setAim(null);
            setIsOpenForm(prev => !prev)
        }catch(error) {
            toast.error(<p className="login__error_msg">{error}</p>, {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
                theme: "light",
              });
              return;
        }
    }
    
    const onClickUpdateForm = (e) => {
        setIsAddGoals(prev => !prev);
        setIsOpenForm(prev => !prev);
        const userId = e.target.attributes.data_id_userId.value
        const attributesdata_id_task = e.target.attributes.data_id_task.value
        const attributesdata_id_progress = e.target.attributes.data_id_progress.value
        const attributesdata_id_description = e.target.attributes.data_id_description.value;
        const attributesdata_id_thumbnail = e.target.attributes.data_id_thumbnail;
        const  attributes_data_id_goals = e.target.attributes.data_id_goalId.value
        setUpdateGoals({
            task: attributesdata_id_task,
            progreess: attributesdata_id_progress,
            description: attributesdata_id_description,
            thumbnail: attributesdata_id_thumbnail,
            goalId: attributes_data_id_goals,
            userId: userId
        });
    }

    const onhandleUpdateGoals = async (e) => {
        e.preventDefault();
        if(!user.isAdmin) {
            toast.error(<p className="login__error_msg">User is not authorized to do this transaction.</p>, {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
                theme: "light",
              });
              return;
        }

        try {
            const response = await axiosPrivate.post("user/updategoals",
            JSON.stringify({
                "userId": user._id,
                "task": updateGoals.task,
                "description": updateGoals.description,
                "progress": updateGoals.proress,
                "thumbnail": updateGoals.thumbnail,
                "goalId": updateGoals.goalId
            }),
            {
                headers: { "Content-Type": "application/json" },
                withCredentials: true
            }
            );

            if(!response.data.success) {
                toast.error(<p className="login__error_msg">{response.data.message}</p>, {
                    position: "top-right",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                    theme: "light",
                  });
                  return;
            }

            const goalsUpdate = goals.map(goal => {
                if(goal._id === updateGoals.goalId) {
                    return {
                        _id: updateGoals.goalId,
                        task: updateGoals.task,
                        progress: updateGoals.progress,
                        description: updateGoals.description,
                        thumbnail: updateGoals.thumbnail
                    }
                }else{
                    return goal;
                }
            });

            setGoals(goalsUpdate);

            setUpdateGoals(null);
            setIsOpenForm(prev => !prev);
            setIsAddGoals(prev => !prev);
        }catch(error) {
            toast.error(<p className="login__error_msg">{error}</p>, {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
                theme: "light",
              });
              return;
        }
    }
    
    const onClickDeleteGoal = async(e) => {
        try {
            const userId = e.target.attributes.data_id_userId.value
            const  attributes_data_id_goals = e.target.attributes.data_id_goalid.value
            const response = await axiosPrivate.post("user/deletegoal",
            JSON.stringify({
                "userId": userId,
                "goalId": attributes_data_id_goals
            }),
            {
                headers: { "Content-Type": "application/json" },
                withCredentials: true
            }
            );

            toast.success(<p className="login__error_msg">{response.data.message}🗑️</p>, {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
                theme: "light",
            });
            

            window.location.reload(true);
        }catch(error) {
            console.log(error);
            toast.error(<p className="login__error_msg">{error}</p>, {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
                theme: "light",
              });
              return;
        }
    }
    return (
    <>
    <ToastContainer />
    <div className="goals__container">
        <div className="goals__container_content">
            <h2 className="goals_header">THIS YEAR GOALS</h2>
            <div className="goals_and_form_container">
                <div className="goals_lists">
                    <div className="goals_addBtn">
                        {
                            user?.isAdmin && 
                            (
                                <>
                                    <div className="goals_addBtn_container" id="goals_btn_desktop">
                                        <i className="fa-solid fa-plus" onClick={() => setIsOpenForm(prev => !prev)}></i>
                                    </div>
                                    <div className="goals_addBtn_container" id={!isOpenForm ? "goals_btn_mobile_show" : "goals_btn_mobile_hide"}>
                                        <i className="fa-solid fa-plus" onClick={() => setIsOpenForm(prev => !prev)}></i>
                                    </div>
                                </>
                            )
                        }
                        {
                            isOpenForm ?
                            (
                                <GoalsForm 
                                    onhandleCreateGoals={onhandleCreateGoals}
                                    handleUpload={handleUpload}
                                    onhandleGoalInputs={onhandleGoalInputs}
                                    onhandleFormSelect={onhandleFormSelect}
                                    status={status}
                                    setIsOpenForm={setIsOpenForm}
                                    isOpenForm={isOpenForm}
                                    isAddGoals={isAddGoals}
                                    setIsAddGoals={setIsAddGoals}
                                    setGoals={setGoals}
                                    updateGoals={updateGoals}
                                    setUpdateGoals={setUpdateGoals}
                                    onUpdateFormSelect={onUpdateFormSelect}
                                    onhandleUpdateGoals={onhandleUpdateGoals}
                                    handleGoalsUploadUpdate={handleGoalsUploadUpdate}
                                />
                            )
                            :
                            (
                            <motion.div ref={carousel} className="carousel_goals_container">
                                <motion.div 
                                    drag="x" 
                                    className="goals_lists_container"
                                    dragConstraints={{ right: 0, /*left: -width*/ }}
                                >
                                {
                                    goals?.map(goal => (
                                        <motion.div 
                                            className="card_container"
                                            data-aos="fade-right"
                                            data-aos-offset="200"
                                            data-aos-delay="30"
                                            data-aos-duration="1500"
                                            data-aos-easing="ease-in-out"
                                            data-aos-mirror="true"
                                            data-aos-once="true"
                                            key={goal._id}
                                        >
                                            {
                                                user.isAdmin &&
                                                (
                                                <div className="goalsBtn">
                                                    <div className="goalsBtn_edit">
                                                        <i 
                                                            className="fa-solid fa-file-pen" 
                                                            onClick={onClickUpdateForm}
                                                            data_id_userId="63c5452695acf197018e4921"
                                                            data_id_task={goal.task}
                                                            data_id_progress={goal.progress}
                                                            data_id_description={goal.description}
                                                            data_id_thumbnail={goal.thumbnail}
                                                            data_id_goalId={goal._id}
                                                        >
                                                        </i>
                                                    </div>
                                                    <div className="goalsBtn_delete">
                                                        <i className="fa-solid fa-circle-xmark"
                                                           onClick={onClickDeleteGoal}
                                                           data_id_userid="63c5452695acf197018e4921"
                                                           data_id_goalid={goal._id}
                                                        ></i>
                                                    </div>
                                                </div>
                                                )
                                            }
                                            <div className="card_image_container">
                                                <div className="iamge_cirle">
                                                    <img src={goal.thumbnail || "https://cdn4.iconfinder.com/data/icons/logos-and-brands/512/144_Gitlab_logo_logos-512.png"} alt="goals_card__image" />
                                                </div>
                                                <p>{goal.task}</p>
                                            </div>
                                            <div className="goals_card__description">
                                                <i className="fa-solid fa-quote-left"></i>
                                                <p>{limitContent(goal.description, 100)}</p>
                                            </div>
                                            <div className="goals_card_status">
                                                <p>{dateFormatConverter(goal.deadline_date)}</p>
                                                <div className="status">
                                                    <p>{goal.progress}</p>
                                                </div>
                                            </div>
                                        </motion.div>
                                    ))
                                }
                                </motion.div>
                            </motion.div>
                            )
                        }
                    </div>
                </div>
            </div>
        </div>
    </div>
    </>
    )
}

export default Goals