import React from 'react'

const GoalsForm = ({
    onhandleCreateGoals,
    handleUpload,
    onhandleGoalInputs,
    onhandleFormSelect,
    status,
    setIsOpenForm,
    isOpenForm,
    isAddGoals,
    setIsAddGoals,
    setGoals,
    updateGoals,
    setUpdateGoals,
    onUpdateFormSelect,
    onhandleUpdateGoals,
    handleGoalsUploadUpdate
}) => {
    const STATUS_ENUMS = ["open", "in-progress", "review", "closed"];
    const onhandleCloseGoalsUpdate = (e) => {
        setIsOpenForm(prev => !prev);
        setIsAddGoals(prev => !prev);
        setUpdateGoals(null);
    }

    const onhandleUpdateGoalInputs = (e) => {
        const values = e.target.value;
        setUpdateGoals({...updateGoals, [e.target.name]: values});
    }
    return (
    <>
        {
            isAddGoals ?
            (
                <div className='goals__form_container'>
                <form className="goals__form_control" onSubmit={onhandleCreateGoals}>
                    <input 
                        type="file"
                        name="thumbnail"
                        onChange={handleUpload}
                        className="thumbnail_uploader"
                    />
                    <div className="input_form_control">
                        <div className="input_division">
                            <label>Task</label>
                            <input 
                                type="text"
                                name="task"
                                onChange={onhandleGoalInputs}
                            />
                        </div>
                        <div className="input_division">
                            <label>Status</label>
                            <select 
                                onChange={onhandleFormSelect}
                                className={
                                    status === "OPEN" ? "open_mocha" : status === "IN-PROGRESS" ? "inprogress_blue" : status === "REVIEW" ? "orange_review" : status === "CLOSED" ? "green_closed" : ""
                                }
                                name="progress"
                            >
                                <option>Goal Status</option>
                                {
                                    STATUS_ENUMS?.map((option, i) => (
                                        <option key={i}>{option.toLocaleUpperCase()}</option>
                                    ))
                                }
                            </select>
                        </div>
                    </div>
                    <div className="textarea_form_control">
                        <div className="input_division">
                            <label>Description</label>
                            <textarea 
                                type="text"
                                name="description"
                                onChange={onhandleGoalInputs}
                            />
                        </div>
                    </div>
                    <div className="formBtn_control">
                        <button onClick={() => setIsOpenForm(prev => !prev) } className="closeBtn" id="closeBtn_desktop">CANCEL</button>
                        <button onClick={() => setIsOpenForm(prev => !prev) } className="closeBtn" id={!isOpenForm ? "closeBtn_mobile_show" : "closeBtn_mobile_hide"}>CANCEL</button>
                        <button className="submitBtn">CREATE</button>
                    </div>
                </form>
            </div>
            )
            :
            (
            <div className='goals__form_container'>
                <form className="goals__form_control" onSubmit={onhandleUpdateGoals}>
                    <input 
                        type="file"
                        name="thumbnail"
                        onChange={handleGoalsUploadUpdate}
                        className="thumbnail_uploader"
                    />
                    <div className="input_form_control">
                        <div className="input_division">
                            <label>Task</label>
                            <input 
                                type="text"
                                name="task"
                                onChange={onhandleUpdateGoalInputs}
                                placeholder={updateGoals.task}
                            />
                        </div>
                        <div className="input_division">
                            <label>Status</label>
                            <select onChange={onUpdateFormSelect}
                                className={
                                    updateGoals.progress === "OPEN" ? "open_mocha" : updateGoals.progress === "IN-PROGRESS" ? "inprogress_blue" : updateGoals.progress === "REVIEW" ? "orange_review" : updateGoals.progress === "CLOSED" ? "green_closed" : ""
                                }
                            >

                                {
                                    STATUS_ENUMS?.map((option, i) => (
                                        <option key={i} selected={updateGoals.progress === option ? true : false}>{option.toLocaleUpperCase()}</option>
                                    ))
                                }
                            </select>
                        </div>
                    </div>
                    <div className="textarea_form_control">
                        <div className="input_division">
                            <label>Description</label>
                            <textarea 
                                type="text"
                                name="description"
                                onChange={onhandleUpdateGoalInputs}
                                placeholder={updateGoals.description}
                            />
                        </div>
                    </div>
                    <div className="formBtn_control">
                        <button onClick={onhandleCloseGoalsUpdate} className="closeBtn" id="closeBtn_desktop">CANCEL</button>
                        <button onClick={onhandleCloseGoalsUpdate} className="closeBtn" id={!isOpenForm ? "closeBtn_mobile_show" : "closeBtn_mobile_hide"}>CANCEL</button>
                        <button className="submitBtn">Submit</button>
                    </div>
                </form>
            </div>
            )
        }
    </>
    )
}

export default GoalsForm