import React from 'react'
import { Link} from "react-scroll";
import useNavbar from '../hooks/useNavbar';
const Navbar = () => {

    const { isNavbarOpen, setIsNavbarOpen } = useNavbar();
    return (
    <div className={isNavbarOpen ? "navbar open" : "navbar"}>
        <div className="navbar_main_menu">
            <div className="navbar__mainlinks">
                <div className="mainlinks_container">
                    <Link
                        activeClass='active'
                        to="homepage"
                        spy={true}
                        smooth={true}
                        offset={-70}
                        duration={500}
                    >HELLO</Link>
                    <Link
                        activeClass='active'
                        to="work__section"
                        spy={true}
                        smooth={true}
                        activeStyle={{
                            fontWeight: 900
                        }}
                        offset={-70}
                        duration={500}
                    >WORK</Link>
                    <Link
                        activeClass='active'
                        to="goals__container"
                        spy={true}
                        smooth={true}
                        activeStyle={{
                            fontWeight: 900
                        }}
                        offset={-70}
                        duration={500}
                    >ABOUT</Link>
                    <Link
                        activeClass='active'
                        to="footer_content_container"
                        spy={true}
                        smooth={true}
                        offset={-70}
                        activeStyle={{
                            fontWeight: 900
                        }}
                        duration={500}
                    >CONTACT</Link>
                </div>
            </div>
            <div className="navbar__social_media">
                <div className="social_media_CTA">
                    <a href="https://www.linkedin.com/in/cyrile-lagumbay-43a4481a8/" id="linkedIn-link"><i className="fa-brands fa-linkedin-in"></i></a>
                    <a href="https://www.facebook.com/cyrile.lagumbay" id="fb-link"><i className="fa-brands fa-facebook-messenger"></i></a>
                    <a href="https://mail.google.com/" id="gmail-link"><i className="fa-regular fa-envelope"></i></a>
                    <a href="#work" id="phone-link"><i className="fa-solid fa-phone"></i></a>
                </div>
            </div>
        </div>
        <div className="navbar__scrollup_icon">
        <Link
            activeClass='active'
            to="homepage"
            spy={true}
            smooth={true}
            offset={-70}
            duration={500}
        ><i className="fa-regular fa-hand-point-up"></i></Link>
        </div>
    </div>
    )
}

export default Navbar