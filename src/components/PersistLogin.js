import { Outlet } from "react-router-dom";
import { useState, useEffect } from "react";
import useRefreshToken from "../hooks/useRefreshToken";
import useAuth from "../hooks/useAuth";
import usePersistingAuth from "../hooks/usePersistingAuth";

const PersistLogin = () => {
    const [isLoading, setIsLoading] = useState(true);
    const refresh = useRefreshToken();
    const { user } = useAuth();
    const persistAuth = usePersistingAuth();

    useEffect(() => {
        let isMounted = true;
        const verifyRefreshToken = async () => {
            try {
                await refresh();
            }catch(error) {
                console.log(error);
            }finally {
                setIsLoading(false);
            }
        }

        if(isMounted) {
            setTimeout(() => {
                if(!user.token) {
                    verifyRefreshToken();
                    persistAuth();
                }else{
                    setIsLoading(false);
                }
            }, 2000)
        }

        return () => isMounted = false;
    }, []);

    return (
        <>
            {
                isLoading
                ? (
                    <div className="loader__container">
                        <div className="loadingio-spinner-pulse-a3d3hvbvx1b">
                            <div className="ldio-ccb43xtdre"><div>
                            </div><div>
                            </div><div>
                            </div></div>
                        </div>
                    </div>
                )
                : <Outlet />
            }
        </>
    )
}

export default PersistLogin;