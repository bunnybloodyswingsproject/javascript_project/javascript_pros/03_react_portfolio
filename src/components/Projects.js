
const Projects = () => {
  return (
    <div className="projects__container" id="work__section">
        <div className="projects">
            <div className="project__card">
                <div className="project__card_left">
                    <p 
                        data-aos="fade-up"
                        data-aos-easing="ease"
                        data-aos-delay="0"
                        data-aos-once="true"
                        data-aos-duration="200"
                    >BATUGSTV - December 2022</p>
                    <div className="project__card_left_content">
                        <h4
                            data-aos="fade-up"
                            data-aos-offset="200"
                            data-aos-delay="0"
                            data-aos-duration="500"
                            data-aos-easing="ease-in-out"
                            data-aos-mirror="true"
                            data-aos-once="true"
                        >INTERACTIVE MUSIC STREAMING PLATFORM</h4>
                        <div className="project_tags_and_description">
                            <div className="project_tags">
                                <div className="tag">
                                    <small>UI/UX Designer</small>
                                </div>
                                <div className="tag">
                                    <small>System Designer</small>
                                </div>
                                <div className="tag">
                                    <small>FullStack Designer</small>
                                </div>
                            </div>
                            <small className="description">BatugsTV is a music/video streaming website is a youtube like clone project that I started last november and finished for about a month.</small>
                        </div>
                        <a href="https://www.figma.com/file/Ykm35Pyutg8FzfPsvlCZT2/BatugsTV?node-id=0%3A1&t=xIhFLGBoahaJRbp9-0" target="__blank">Explore Figma</a>
                    </div>
                </div>
                <div className="project__card_right">
                    <img 
                        src="https://firebasestorage.googleapis.com/v0/b/netflix-f5fc3.appspot.com/o/batugst_file_uploader%2FHomepage.png?alt=media&token=276fed8f-fc47-459a-b467-b7d08d20e538" 
                        alt="project_card_right_img" 
                        data-aos="fade-left"
                        data-aos-offset="200"
                        data-aos-delay="0"
                        data-aos-duration="300"
                        data-aos-easing="ease-in-out"
                        data-aos-mirror="true"
                        data-aos-once="true"
                    />
                </div>
            </div>
        </div>
        <div className="projects">
            <div className="project__card" id="sayuji">
                <div className="project__card_left">
                    <p
                        data-aos="fade-up"
                        data-aos-easing="ease"
                        data-aos-delay="0"
                        data-aos-once="true"
                        data-aos-duration="200"
                    >SAYUJI - February 2021</p>
                    <div className="project__card_left_content">
                        <h4
                            data-aos="fade-up"
                            data-aos-offset="200"
                            data-aos-delay="0"
                            data-aos-duration="500"
                            data-aos-easing="ease-in-out"
                            data-aos-mirror="true"
                            data-aos-once="true"
                        >E-COMMERCE PLATFORM FOR AVID SHOPPERS</h4>
                        <div className="project_tags_and_description">
                            <div className="project_tags" id="project_tags_sayuji">
                                <div className="tag">
                                    <small>UI/UX Designer</small>
                                </div>
                                <div className="tag">
                                    <small>System Designer</small>
                                </div>
                                <div className="tag">
                                    <small>FullStack Designer</small>
                                </div>
                            </div>
                            <small className="description">SAYUJI is a website that I tried to build for my relatives back in 2021. It consists of full shopping experience build from PHP.</small>
                        </div>
                        <a href="https://www.figma.com/file/TPfECsFtIENVpU2w7CFOD7/Celigina?t=xIhFLGBoahaJRbp9-0" target="__blank" id="sayujiBTN">Explore Figma</a>
                    </div>
                </div>
                <div className="project__card_right">
                    <img 
                        src="https://firebasestorage.googleapis.com/v0/b/netflix-f5fc3.appspot.com/o/batugst_file_uploader%2Fright_info-removebg-preview.png?alt=media&token=fb26bdd9-69f1-4b3f-a7cc-4dbcc85044ee" 
                        alt="project_card_right_img" 
                        data-aos="fade-up"
                        data-aos-offset="200"
                        data-aos-delay="0"
                        data-aos-duration="300"
                        data-aos-easing="ease-in-out"
                        data-aos-mirror="true"
                        data-aos-once="true"
                    />
                </div>
            </div>
        </div>
        <div className="projects">
            <div className="project__card" id="blitz">
                <div className="project__card_left">
                    <p
                        data-aos="fade-up"
                        data-aos-easing="ease"
                        data-aos-delay="0"
                        data-aos-once="true"
                        data-aos-duration="200"
                    >BLITZ - July 2022</p>
                    <div className="project__card_left_content">
                        <h4
                            data-aos="fade-up"
                            data-aos-offset="200"
                            data-aos-delay="0"
                            data-aos-duration="500"
                            data-aos-easing="ease-in-out"
                            data-aos-mirror="true"
                            data-aos-once="true"
                        >ALTERNATIVE STREAMING PLATFORM FOR NETFLIX</h4>
                        <div className="project_tags_and_description">
                            <div className="project_tags" id="project_tags_blitz">
                                <div className="tag">
                                    <small>UI/UX Designer</small>
                                </div>
                                <div className="tag">
                                    <small>System Designer</small>
                                </div>
                                <div className="tag">
                                    <small>FullStack Designer</small>
                                </div>
                            </div>
                            <small className="description">BLITZ is a alternative streaming platform for netflix. Due to the popularity of netflix, watching movie would be a expensive hobby for streming users.</small>
                        </div>
                        <a href="https://www.figma.com/file/DsTNlIVXWJHxF0iaasGpCD/Video-Streaming-App?node-id=14%3A22&t=xIhFLGBoahaJRbp9-0" target="__blank" id="blitzBTN">Explore Figma</a>
                    </div>
                </div>
                <div className="project__card_right">
                    <img 
                        src="https://firebasestorage.googleapis.com/v0/b/netflix-f5fc3.appspot.com/o/batugst_file_uploader%2FVIDEO%20DESCRIPTIONblitz.png?alt=media&token=13091681-d184-4345-8719-57abb1d897cb" 
                        alt="project_card_right_img" 
                        data-aos="fade-left"
                        data-aos-offset="200"
                        data-aos-delay="0"
                        data-aos-duration="300"
                        data-aos-easing="ease-in-out"
                        data-aos-mirror="true"
                        data-aos-once="true"
                    />
                </div>
            </div>
        </div>
        <div className="projects">
            <div className="project__card" id="okbet">
                <div className="project__card_left">
                    <p
                        data-aos="fade-up"
                        data-aos-easing="ease"
                        data-aos-delay="0"
                        data-aos-once="true"
                        data-aos-duration="200"
                    >OKBET - January 2023</p>
                    <div className="project__card_left_content">
                        <h4
                            data-aos="fade-up"
                            data-aos-offset="200"
                            data-aos-delay="0"
                            data-aos-duration="500"
                            data-aos-easing="ease-in-out"
                            data-aos-mirror="true"
                            data-aos-once="true"
                        >The Premier Destination for Sports Betting</h4>
                        <div className="project_tags_and_description">
                            <div className="project_tags" id="project_tags_blitz">
                                <div className="tag">
                                    <small>UI/UX Designer</small>
                                </div>
                                <div className="tag">
                                    <small>System Designer</small>
                                </div>
                                <div className="tag">
                                    <small>FullStack Designer</small>
                                </div>
                            </div>
                            <small className="description">OKBET - Your trusted source for sports betting odds and expert analysis. Join now to get the best odds on your favorite sports events.</small>
                        </div>
                        <a href="https://okbet-gaming.onrender.com/" target="__blank" id="blitzBTN">Check Website</a>
                    </div>
                </div>
                <div className="project__card_right">
                    <img 
                        src="https://firebasestorage.googleapis.com/v0/b/practicefreewebsitehosting.appspot.com/o/dice_image.jpg?alt=media&token=b63fadbe-474b-4285-81fc-7c6bc8c7d74f" 
                        alt="project_card_right_img" 
                        data-aos="fade-left"
                        data-aos-offset="200"
                        data-aos-delay="0"
                        data-aos-duration="300"
                        data-aos-easing="ease-in-out"
                        data-aos-mirror="true"
                        data-aos-once="true"
                    />
                </div>
            </div>
        </div>
    </div>
  )
}

export default Projects