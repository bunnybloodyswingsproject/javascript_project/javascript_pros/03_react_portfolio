import { createContext, useState } from "react";

export const NavbarContext = createContext(false);
export const NavbarProvider = ({children}) => {
    const [isNavbarOpen, setIsNavbarOpen] = useState(false);

    return (
        <NavbarContext.Provider value={{isNavbarOpen, setIsNavbarOpen}}>
            { children }
        </NavbarContext.Provider>
    )
}