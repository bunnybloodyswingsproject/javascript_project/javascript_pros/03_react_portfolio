import {initializeApp} from "firebase/app";
import {getStorage} from "firebase/storage";

const firebaseConfig = {
  apiKey: "AIzaSyDqFSfI0kQX0DBMEgrhu89TEJNsy7fzAHM",
  authDomain: "netflix-f5fc3.firebaseapp.com",
  projectId: "netflix-f5fc3",
  storageBucket: "netflix-f5fc3.appspot.com",
  messagingSenderId: "550764183863",
  appId: "1:550764183863:web:61a646cd906ee2867ddab2",
  measurementId: "G-QZ1TD19HJY"
};

const firebase = initializeApp(firebaseConfig);
const storage = getStorage(firebase);

  export default storage;
