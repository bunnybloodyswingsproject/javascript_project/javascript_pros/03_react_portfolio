import { useContext } from "react";
import { AuthContext } from "../context/UserProvider";

const useAuth = () => {
    return useContext(AuthContext);
}

export default useAuth;