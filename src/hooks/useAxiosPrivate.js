import axios from 'axios'
import { useEffect } from 'react'
import useAuth from './useAuth';
import useRefreshToken from "./useRefreshToken";

const useAxiosPrivate = () => {
    const axiosPrivate = axios.create({
        baseURL: process.env.REACT_APP_API_URL,
        headers: { "Content-Type": "application/json" },
        withCredentials: true
    });

    const refresh = useRefreshToken();
    const {user} = useAuth();

    useEffect(() => {
        const requestIntercept = axiosPrivate.interceptors.request.use(
            config => {
                if(!config.headers["Authorization"]) {
                    config.headers["Authorization"] = `Bearer ${user.token}`;
                }

                return config;
            }, (error) => Promise.reject(error)
        );

        const responseIntercept = axiosPrivate.interceptors.response.use(
            response => response,
            async (error) => {
                console.log("responseInterceptor error", error)
                const prevRequest = error?.config;
                if(error?.response?.status === 403 && !prevRequest?.sent) {
                    prevRequest.send = true;
                    const newAccessToken = await refresh();
                    prevRequest.headers["Authorization"] = `Bearer ${newAccessToken}`;
                    return axiosPrivate(prevRequest);
                }
                return Promise.reject(error);
            }
        );

        return () => {
            axiosPrivate.interceptors.request.eject(requestIntercept);
            axiosPrivate.interceptors.response.eject(responseIntercept);
        }
    }, [user, refresh])
  return axiosPrivate;
}

export default useAxiosPrivate