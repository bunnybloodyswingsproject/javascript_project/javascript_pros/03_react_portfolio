import { useContext } from "react";
import { GoalsContext } from "../context/GoalsProvider";

const useGoals = () => {
    return useContext(GoalsContext);
}

export default useGoals;