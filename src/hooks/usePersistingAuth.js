import axios from "axios";
import { useNavigate } from "react-router-dom";
import useAuth from "./useAuth";

 const usePersistingAuth = () => {
    const { user, setUser } = useAuth();
    const navigate = useNavigate();
    const axiosInstance = axios.create({
        baseURL: process.env.REACT_APP_API_URL
    });

    const persistingAuth = async () => {
        try {
            const response = await axiosInstance.get("/user/persistingauth", {
                withCredentials: true
            });
            if(response.data.user === null) {
                alert("Fetching of data failed. Please login again.");
                navigate("/login");
            }
            const {
                _id,
                fullname,
                email,
                image,
                isAdmin,
                goals
            } = response.data.user;
            setUser(prev => {
                return {
                    ...prev,
                    "_id": _id,
                    "fullname": fullname,
                    "email": email,
                    "image": image,
                    "isAdmin": isAdmin,
                    "goals": goals
                }
            });
            return user;
        }catch(error) {
            console.log(error);
            navigate("/");
        }
    }
    return persistingAuth;
 }

 export default usePersistingAuth;