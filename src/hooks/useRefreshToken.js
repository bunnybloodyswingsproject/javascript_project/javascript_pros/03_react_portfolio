import axios from "axios";
import useAuth from "./useAuth";

const useRefreshToken = () => {
    const { setUser } = useAuth();
    const axiosInstance = axios.create({
        baseURL: process.env.REACT_APP_API_URL
    });

    const refresh = async () => {
        const response = await axiosInstance.get("/user/refresh", {
            withCredentials: true
        });
        setUser(prev => {
            return {...prev, token: response.data.token}
        });

        return response.data.token;
    }
    return refresh
}

export default useRefreshToken;