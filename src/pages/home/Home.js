import Certificates from "../../components/Certificates";
import Footer from "../../components/Footer";
import Goals from "../../components/Goals";
import Navbar from "../../components/Navbar";
import Projects from "../../components/Projects";
import "./home.css";
import AOS from "aos";
import { useEffect, useState } from "react";
import TypeIt from "typeit-react";
import useNavbar from "../../hooks/useNavbar";
import useAuth from "../../hooks/useAuth";

const Home = () => {
    const { user, setUser } = useAuth();
    useEffect(() => {
        AOS.init();
    }, []);

    const roles = ["FULLSTACK ", "FRONTEND ", "UI/UX "];
    const mobile_roles = ["CYRILE LAGUMBAY", "FULLSTACK DEVELOPER", "FRONTEND DEVELOPER", "UI/UX DEVELOPER"];
    const { setIsNavbarOpen, isNavbarOpen } = useNavbar();
    const [navbarColor, setNavbarColor] = useState(false);
    const navbarChangeBackground = () => {
        window.scrollY > 1 ? setNavbarColor(true) : setNavbarColor(false);
    }

    window.addEventListener("scroll", navbarChangeBackground);
    return (
    <>
    <Navbar />
    <div className="homepage" id="homepage">
        <div className="main__section">
            <div className="main__section_left_banner">
                <div className="main__section_text_banner">
                    <div className="main__section_text_banner_header">
                        <div 
                            className={isNavbarOpen ? "mobile__navbar open" : "mobile__navbar"}
                            style={{
                                backgroundColor: navbarColor ? "blanchedalmond" : ""
                            }}
                        >
                            <div className="navbar__mobile_brand">
                                    <p className="mobile_role__animation">
                                        Hi I`M <TypeIt 
                                            getBeforeInit={(instance) => {
                                                for(let i = 0; i < 100; i++) {
                                                    instance
                                                    .type(mobile_roles[0])
                                                    .pause(2000)
                                                    .delete(mobile_roles[0].length)
                                                    .pause(500)
                                                    .type(mobile_roles[1])
                                                    .pause(2000)
                                                    .delete(mobile_roles[1].length)
                                                    .type(mobile_roles[1])
                                                    .pause(2000)
                                                    .delete(mobile_roles[1].length)
                                                    .type(mobile_roles[2])
                                                    .pause(2000)
                                                    .delete(mobile_roles[2].length)
                                                    .type(mobile_roles[3])
                                                    .pause(2000)
                                                    .delete(mobile_roles[3].length)
                                                }
                                                return instance;
                                            }}
                                        />
                                    </p>
                                <span>👋</span>
                            </div>
                            <div className="navbar__mobile_burger">
                                { !isNavbarOpen && <i className="fa-solid fa-bars" onClick={() => setIsNavbarOpen(prev => !prev)}></i> }
                                { isNavbarOpen && <i className="fa-solid fa-xmark" onClick={() => setIsNavbarOpen(prev => !prev)}></i> }
                            </div>
                        </div>
                        <p className="greetings__from_me">HI I`M CYRILE LAGUMBAY 👋</p>
                        <h4>
                            <span className="role__animation">
                                <TypeIt 
                                    getBeforeInit={(instance) => {
                                        for(let i = 0; i < 100; i++) {
                                            instance
                                            .type(roles[0])
                                            .pause(2000)
                                            .delete(roles[0].length)
                                            .pause(500)
                                            .type(roles[1])
                                            .pause(2000)
                                            .delete(roles[1].length)
                                            .type(roles[1])
                                            .pause(2000)
                                            .delete(roles[1].length)
                                            .type(roles[2])
                                            .pause(2000)
                                            .delete(roles[2].length)
                                        }
                                        return instance;
                                    }}
                                /> DEVELOPER
                            </span>
                            <span className="role_subtext">focused on creating awesome Innovative, and artistic user Interface.</span>
                        </h4>
                    </div>
                    <div className="main__section_text_banner_header_underline"></div>
                </div>
                <div className="main__section_subtext_banner">
                    <h4>Currently working at <a href="https://www.prulifeuk.com.ph/en/" target="__blank">PRULIFEUK</a>.</h4>
                </div>
            </div>
            <div className="main__section_right_banner">
                <img 
                    src="https://firebasestorage.googleapis.com/v0/b/netflix-f5fc3.appspot.com/o/batugst_file_uploader%2Fasdsad-removebg-preview%20(1).png?alt=media&token=5c8ed162-12b1-49c5-ad4e-1f8ebe5811bc" 
                    alt="homepage_main_banner" 
                    data-aos="fade-left"
                    data-aos-offset="200"
                    data-aos-delay="50"
                    data-aos-duration="1000"
                    data-aos-easing="ease-in-out"
                    data-aos-mirror="true"
                    data-aos-once="true"
                />
            </div>
        </div>
        <div className="work__section">
            <div className="work__section_container">
                <p className="work__section_header_link">PROJECTS INVOLVEMENT</p>
                <Projects />
                <Certificates />
                <Goals user={user} setUser={setUser} />
                <Footer />
            </div>
        </div>
    </div>
    </>
    )
}

export default Home