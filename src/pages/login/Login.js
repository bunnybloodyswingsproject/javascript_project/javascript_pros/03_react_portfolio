import React, { useEffect, useRef, useState } from 'react'
import { useLocation, useNavigate } from 'react-router-dom';
import useAuth from '../../hooks/useAuth';
import "./login.css";
import axios from "axios";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const Login = () => {
  const { setUser } = useAuth();
  const location = useLocation();
  const navigate = useNavigate();
  const from = location.state?.from?.pathname || "/";
  const emailRef = useRef();
  const [userCredentials, setUserCredentials] = useState(null);
  const axiosInstance = axios.create({
    baseURL: process.env.REACT_APP_API_URL
  });
  const onChangeUserCredentials = (e) => {
    const value = e.target.value;
    setUserCredentials({...userCredentials, [e.target.name]: value});
  }

  useEffect(() => {
    emailRef.current.focus();
    return () => console.log("email focus dismount.")
  }, []);

  const handleLoginSubmit = async (e) => {
    e.preventDefault();

    try {
      const response = await axiosInstance.post("auth/login",
      JSON.stringify({
        email: userCredentials.email,
        password: userCredentials.password
      }), {
        headers: { "Content-Type": "application/json" },
        withCredentials: true
      });
      if(!response.data.success) {
        toast.error(<p className="login__error_msg">{response.data.message}</p>, {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
          theme: "light",
        });
        return;
        // console.log("Printing error for the meantime.");
      }


      setUser(response.data.user);
      setUserCredentials(null);
      navigate(from, { replace: true });

    }catch(error) {
      console.log(error);
    }
  }

  return (
    <>
      <ToastContainer />
      <div className="login__container">
        <div className="login_content">
          <div className="login_box">
            <div className="login_image_container">
              <img src="https://firebasestorage.googleapis.com/v0/b/netflix-f5fc3.appspot.com/o/batugst_file_uploader%2Fasdsad-removebg-preview%20(1).png?alt=media&token=5c8ed162-12b1-49c5-ad4e-1f8ebe5811bc" alt="login_image" />
            </div>
            <form className='login_forms' onSubmit={handleLoginSubmit}>
              <div className="login_form_control">
                <label>USERNAME</label>
                <input 
                  type="text"
                  ref={emailRef}
                  name="email"
                  onChange={onChangeUserCredentials}
                />
              </div>
              <div className="login_form_control">
                <label>PASSWORD</label>
                <input 
                  type="password"
                  name="password"
                  onChange={onChangeUserCredentials}
                />
              </div>
              <div className="loginBtn_container">
                <button>LOGIN</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </>
  )
}

export default Login